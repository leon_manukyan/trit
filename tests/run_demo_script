#!/usr/bin/env bash

myself="$0"
myname="$(basename "$myself")"

if [ "$1" == '--batch' ]
then
    demo_script_batch_mode=yes
    shift
fi

if [ $# -ne 1 ]
then
    echo 1>&2 "Usage: $myname [--batch] <demo_script>"
    exit 1
fi

demo_script="$1"
if [ "$demo_script" != "$myself" ]
then
    exec bash -c "demo_script_batch_mode=$demo_script_batch_mode; . $myself" "$demo_script" "$@"
fi

declare -a demo_script_lines
let i=0
while IFS='' read -r line
do
    if [[ $i -eq 0 && $line == '#!'* ]]; then continue; fi

    demo_script_lines[i]="$line"
    let ++i
done < "$demo_script"

demo_script_block=""
demo_script_block_pause=yes
for demo_script_current_line in "${demo_script_lines[@]}"
do
    if [ -z "$demo_script_current_line" ]
    then
        if [ -z "$demo_script_block" ]; then continue; fi

        if [ "$demo_script_block_pause" == yes ]
        then
            printf '\n\n\n%s' "$demo_script_block"
            if [ "$demo_script_batch_mode" != yes ]
            then
                printf "#          ( Press ENTER to continue )"
                read
            fi
        fi
        eval "$demo_script_block"
        demo_script_block=""
        demo_script_block_pause=yes
    else
        if [ "$demo_script_current_line" == "# NOPAUSE" ]
        then
            demo_script_block_pause=no
        fi
        demo_script_block+="$demo_script_current_line"$'\n'
    fi
done

if [ -n "$demo_script_block" ]
then
    if [ "$demo_script_block_pause" == yes ]
    then
        printf '\n\n\n%s' "$demo_script_block"
        if [ "$demo_script_batch_mode" != yes ]
        then
            printf "#          ( Press ENTER to continue )"
            read
        fi
    fi
    eval "$demo_script_block"
fi
