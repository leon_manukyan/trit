#!/usr/bin/env bash

git clone --no-hardlinks tritdir/tests/purl

cd purl

################################################################################
# Negative tests for command syntax checking
################################################################################

# 'trit init' accepts at most one argument
trit init abc def

# 'trit init' should reject an argument that looks like an option
trit init --anoption

# 'trit bugs' doesn't accept any arguments
trit bugs something

# 'trit showbug' requires a single argument
trit showbug

# 'trit showbug' requires a single argument
trit showbug somebug something

# A single argument that looks like an option must be rejected
trit showbug --someoption

# 'trit addbug' requires a single argument
trit addbug

# 'trit addbug' requires a single argument
trit addbug somebug something

# A single argument that looks like an option must be rejected
trit addbug --someoption

# 'trit checkbugs' doesn't accept any options
trit checkbugs --anoption

# 'trit stat' doesn't accept any arguments
trit stat something

# 'trit catchup' doesn't accept any arguments
trit catchup something


################################################################################
# 'trit init' must be called on a branch before any other trit command can
# be used on that branch
################################################################################

# 'trit addbug' cannot work without 'trit init'
trit addbug somebug

# 'trit bugs' cannot work without 'trit init'
trit bugs

# 'trit showbug' cannot work without 'trit init'
trit showbug somebug

# 'trit checkbugs' cannot work without 'trit init'
trit checkbugs

# 'trit stat' cannot work without 'trit init'
trit stat

# 'trit catchup' cannot work without 'trit init'
trit catchup


################################################################################
# 'trit init':
################################################################################

# Introduce a non-functional change
echo '#' >> purl/url.py

# Call 'trit init' on a dirty working tree
trit init ./run_tests

# Clean the working tree
git checkout .

# Make sure that there is no run_tests file
ls -lgGd --time-style=+'' run_tests

# Call 'trit init' with non-existent test running file
trit init ./run_tests

mkdir ./run_tests
ls -lgGd --time-style=+'' run_tests

# Pass a directory to 'trit init'
trit init ./run_tests

rmdir ./run_tests

# Create a fake always-failing "testing" script
cat > run_tests.always_fails <<'END'
#!/usr/bin/env bash
exit 1
END

# Set up a symbolic link
ln -s run_tests.always_fails run_tests.symlink
ls -lgGd --time-style=+'' run_tests.symlink

# 'trit init' should refuse to accept a symbolic link
trit init ./run_tests.symlink

rm run_tests.symlink

# 'trit init' shouldn't accept a non-executable test running script
chmod -x run_tests.always_fails
trit init ./run_tests.always_fails

# 'trit init' shouldn't accept a testing script that fails on clean working tree
chmod +x run_tests.always_fails
trit init ./run_tests.always_fails

rm run_tests.always_fails

# Create the real test running script
cat > run_tests <<'END'
#!/usr/bin/env bash
set -o pipefail
testfilter=tests.url_tests:SimpleExtractionTests
nosetests --no-byte-compile -v "$testfilter" 2>&1 \
| sed "s/ (${testfilter/:/.}) ... /: /" \
| sed '
    /^Ran [0-9]\{1,\} tests in/ {s/^/------------\'$'\n''/; s/ in .\{1,\}//;p;};
    /^OK$/ p;
    /^FAILED (errors=[0-9]\{1,\}, failures=[0-9]\{1,\})$/ p;
    /^FAILED (failures=[0-9]\{1,\})$/ p;
    /^FAILED (errors=[0-9]\{1,\})$/ p;
    /^$/,$ d'
END
chmod +x run_tests

# No more reasons to complain
trit init ./run_tests

# Check that master.trit branch was created (but we are back on master)
git branch --list

# ... and its branchpoint was tagged
git tag -l '*.trit.root' -n

# Check the commit message of 'trit init'
git log --format=format:%B -n 1 master.trit

# Make sure that the run_tests script has disappeared and the work tree is clean
git status --porcelain --branch





# 'trit bugs' shouldn't show anything when there are no bugs
trit bugs

# 'trit showbug' shouldn't be confused by a non-existent bug
trit showbug nosuchbug

# Introduce a bug
cat >> purl/url.py <<'END'
origparse=parse
def parse(url_str):
    return origparse(url_str.replace('?',''))
END

# ... and tell trit about it
trit addbug parse_with_question_mark_dropped

# Make sure that we are left on the master branch with a clean working tree
git status --porcelain --branch

# Now there IS a bug to show
trit bugs

# We should be able to examine it
trit showbug parse_with_question_mark_dropped

# Appearance of a bug doesn't mean that it can be presented as any bug
trit showbug nosuchbug

# Now let's try to cheat trit by adding a bug that doesn't break anything
git checkout purl/url.py
echo >> purl/url.py '# an extra comment line'
trit addbug "Not_a_bug"

# Check that the change didn't disappear
git status --porcelain --branch

# Check that the 'bug' was not added
trit bugs

# Check that it really wasn't added
trit showbug Not_a_bug

# Check that the previous bug was not affected
trit showbug parse_with_question_mark_dropped

# Now create another bug
git checkout purl/url.py
cat >> purl/url.py <<'END'
origparse=parse
def parse(url_str):
    return origparse(url_str[:-1])
END
trit addbug parse_with_last_symbol_dropped

git status --porcelain --branch

trit bugs

trit showbug parse_with_last_symbol_dropped

trit showbug parse_with_question_mark_dropped

# Check that the newly added bug's status
trit checkbugs parse_with_question_mark_dropped

# Verify that 'trit checkbugs' doesn't resurrect the bug
git status --porcelain --branch

# Rename a couple of tests
sed -i -e 's/test_subdomain/test_sub_domain/' tests/url_tests.py
git status --porcelain --branch

# 'trit checkbugs' should detect that the effect
# of the given bug on the tests has changed
trit checkbugs parse_with_question_mark_dropped

# 'trit checkbugs' should preserve the local changes
git status --porcelain --branch

# Without arguments, 'trit checkbugs' should check all the bugs
trit checkbugs

# Drop the local change
git checkout tests/url_tests.py

# Now let's do some 'development' and checkin some code
echo '#' >> purl/template.py
git commit -qm "Non functional change in purl/template.py" purl/template.py

# 'trit bugs' must still report the same bugs
trit bugs

# An attempt to add a bug after the code has changed should be rejected
git checkout purl/url.py
sed -i -e 's/urlparse(url_str)/urlparse(url_str.replace("www", "abc"))/' purl/url.py
trit addbug www_replaced_with_abc

# No traces of the failed attempt to add a bug should show up
trit bugs

# 'trit catchup' should not require any intervention in this case
trit catchup

# No new branches should appear as a result of completed 'trit catchup'
git branch --list

# No new tags should appear as a result of completed 'trit catchup'
git tag -l '*trit*' -n

# No bugs should disappear as a result of 'trit catchup'
trit bugs

# 'trit checkbugs' should work normally after 'trit catchup'
trit checkbugs

# Now 'trit addbug' should not complain of anything
trit addbug www_replaced_with_abc

git branch --list

trit bugs

trit showbug www_replaced_with_abc

trit showbug parse_with_last_symbol_dropped

trit showbug parse_with_question_mark_dropped

trit checkbugs parse_with_last_symbol_dropped www_replaced_with_abc

# 'trit stat' reports statistics of test failures due to different bugs
trit stat
