


git clone --no-hardlinks tritdir/tests/purl
Cloning into 'purl'...
done.



cd purl



################################################################################
# Negative tests for command syntax checking
################################################################################



# 'trit init' accepts at most one argument
trit init abc def
ERROR: Usage: trit init [<testing_script>]



# 'trit init' should reject an argument that looks like an option
trit init --anoption
ERROR: Unknown option: --anoption
ERROR: Usage: trit init [<testing_script>]



# 'trit bugs' doesn't accept any arguments
trit bugs something
ERROR: Usage: trit bugs



# 'trit showbug' requires a single argument
trit showbug
ERROR: Usage: trit showbug <bugid>



# 'trit showbug' requires a single argument
trit showbug somebug something
ERROR: Usage: trit showbug <bugid>



# A single argument that looks like an option must be rejected
trit showbug --someoption
ERROR: Unknown option: --someoption
ERROR: Usage: trit showbug <bugid>



# 'trit addbug' requires a single argument
trit addbug
ERROR: Usage: trit addbug <bugid>



# 'trit addbug' requires a single argument
trit addbug somebug something
ERROR: Usage: trit addbug <bugid>



# A single argument that looks like an option must be rejected
trit addbug --someoption
ERROR: Unknown option: --someoption
ERROR: Usage: trit addbug <bugid>



# 'trit checkbugs' doesn't accept any options
trit checkbugs --anoption
ERROR: Unknown option: --anoption
ERROR: Usage: trit checkbugs [<bugid> ...]



# 'trit stat' doesn't accept any arguments
trit stat something
ERROR: Usage: trit stat



# 'trit catchup' doesn't accept any arguments
trit catchup something
ERROR: Usage: trit catchup



################################################################################
# 'trit init' must be called on a branch before any other trit command can
# be used on that branch
################################################################################



# 'trit addbug' cannot work without 'trit init'
trit addbug somebug
ERROR: 'trit init' was never called on branch master



# 'trit bugs' cannot work without 'trit init'
trit bugs
ERROR: 'trit init' was never called on branch master



# 'trit showbug' cannot work without 'trit init'
trit showbug somebug
ERROR: 'trit init' was never called on branch master



# 'trit checkbugs' cannot work without 'trit init'
trit checkbugs
ERROR: 'trit init' was never called on branch master



# 'trit stat' cannot work without 'trit init'
trit stat
ERROR: 'trit init' was never called on branch master



# 'trit catchup' cannot work without 'trit init'
trit catchup
ERROR: 'trit init' was never called on branch master



################################################################################
# 'trit init':
################################################################################



# Introduce a non-functional change
echo '#' >> purl/url.py



# Call 'trit init' on a dirty working tree
trit init ./run_tests
ERROR: Working tree is not clean. The following files are modified:
ERROR: - purl/url.py
ERROR: 
ERROR: 'trit init' must be executed inside a clean working tree.



# Clean the working tree
git checkout .



# Make sure that there is no run_tests file
ls -lgGd --time-style=+'' run_tests
ls: cannot access 'run_tests': No such file or directory



# Call 'trit init' with non-existent test running file
trit init ./run_tests
ERROR: No such file: ./run_tests



mkdir ./run_tests
ls -lgGd --time-style=+'' run_tests
drwxrwxr-x 2 4096  run_tests



# Pass a directory to 'trit init'
trit init ./run_tests
ERROR: Not a regular file: ./run_tests



rmdir ./run_tests



# Create a fake always-failing "testing" script
cat > run_tests.always_fails <<'END'
#!/usr/bin/env bash
exit 1
END



# Set up a symbolic link
ln -s run_tests.always_fails run_tests.symlink
ls -lgGd --time-style=+'' run_tests.symlink
lrwxrwxrwx 1 22  run_tests.symlink -> run_tests.always_fails



# 'trit init' should refuse to accept a symbolic link
trit init ./run_tests.symlink
ERROR: Not a regular file: ./run_tests.symlink



rm run_tests.symlink



# 'trit init' shouldn't accept a non-executable test running script
chmod -x run_tests.always_fails
trit init ./run_tests.always_fails
ERROR: Not an executable file: ./run_tests.always_fails



# 'trit init' shouldn't accept a testing script that fails on clean working tree
chmod +x run_tests.always_fails
trit init ./run_tests.always_fails
ERROR: Exit status of ./run_tests.always_fails doesn't indicate success



rm run_tests.always_fails



# Create the real test running script
cat > run_tests <<'END'
#!/usr/bin/env bash
set -o pipefail
testfilter=tests.url_tests:SimpleExtractionTests
nosetests --no-byte-compile -v "$testfilter" 2>&1 \
| sed "s/ (${testfilter/:/.}) ... /: /" \
| sed '
    /^Ran [0-9]\{1,\} tests in/ {s/^/------------\'$'\n''/; s/ in .\{1,\}//;p;};
    /^OK$/ p;
    /^FAILED (errors=[0-9]\{1,\}, failures=[0-9]\{1,\})$/ p;
    /^FAILED (failures=[0-9]\{1,\})$/ p;
    /^FAILED (errors=[0-9]\{1,\})$/ p;
    /^$/,$ d'
END
chmod +x run_tests



# No more reasons to complain
trit init ./run_tests
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: ok
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: ok
test_query_param_as_list: ok
test_query_params: ok
test_relative: ok
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
OK

Please review the test run results above and
make sure that all tests of interest are listed.

Do you want to proceed with 'trit init'? (y/Y/n/N)



# Check that master.trit branch was created (but we are back on master)
git branch --list
* master
  master.trit



# ... and its branchpoint was tagged
git tag -l '*.trit.root' -n
master.trit.root Root of the trit branch for master



# Check the commit message of 'trit init'
git log --format=format:%B -n 1 master.trit
Initial commit on master.trit

./run_tests
-----------------------------
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: ok
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: ok
test_query_param_as_list: ok
test_query_params: ok
test_relative: ok
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
OK



# Make sure that the run_tests script has disappeared and the work tree is clean
git status --porcelain --branch
## master...origin/master



# 'trit bugs' shouldn't show anything when there are no bugs
trit bugs



# 'trit showbug' shouldn't be confused by a non-existent bug
trit showbug nosuchbug
ERROR: No such bug: nosuchbug



# Introduce a bug
cat >> purl/url.py <<'END'
origparse=parse
def parse(url_str):
    return origparse(url_str.replace('?',''))
END



# ... and tell trit about it
trit addbug parse_with_question_mark_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..6527bde 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -521,10 +521,13 @@ class URL(object):
         return cls(**args)
 
     @classmethod
     def from_string(cls, url_str):
         """
         Factory method to create a new instance based on a passed string
 
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str.replace('?',''))
====================================================

Please review the bug code above and make sure that

 1) there are no changes unrelated to the intended bug

 2) all changes are in the code rather than in tests

Do you want to proceed with 'trit addbug'? (y/Y/n/N)
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: FAIL
test_has_query_params: FAIL
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: FAIL
test_path_extraction: FAIL
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: FAIL
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ERROR
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (errors=1, failures=10)
====================================================

Please review the effect of the bug on the tests above.

Have all tests that this bug should have broken failed? (y/Y/n/N)



# Make sure that we are left on the master branch with a clean working tree
git status --porcelain --branch
## master...origin/master



# Now there IS a bug to show
trit bugs
parse_with_question_mark_dropped  (errors=1, failures=10)



# We should be able to examine it
trit showbug parse_with_question_mark_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..6527bde 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -528,3 +528,6 @@ class URL(object):
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str.replace('?',''))
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: FAIL
test_has_query_params: FAIL
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: FAIL
test_path_extraction: FAIL
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: FAIL
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ERROR
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (errors=1, failures=10)



# Appearance of a bug doesn't mean that it can be presented as any bug
trit showbug nosuchbug
ERROR: No such bug: nosuchbug



# Now let's try to cheat trit by adding a bug that doesn't break anything
git checkout purl/url.py
echo >> purl/url.py '# an extra comment line'
trit addbug "Not_a_bug"
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..f17f586 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -521,10 +521,11 @@ class URL(object):
         return cls(**args)
 
     @classmethod
     def from_string(cls, url_str):
         """
         Factory method to create a new instance based on a passed string
 
         This method is deprecated now
         """
         return cls(url_str)
+# an extra comment line
====================================================

Please review the bug code above and make sure that

 1) there are no changes unrelated to the intended bug

 2) all changes are in the code rather than in tests

Do you want to proceed with 'trit addbug'? (y/Y/n/N)
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: ok
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: ok
test_query_param_as_list: ok
test_query_params: ok
test_relative: ok
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
OK
====================================================

ERROR: Tests passed without any problem. Review the bug or the tests.



# Check that the change didn't disappear
git status --porcelain --branch
## master...origin/master
 M purl/url.py



# Check that the 'bug' was not added
trit bugs
parse_with_question_mark_dropped  (errors=1, failures=10)



# Check that it really wasn't added
trit showbug Not_a_bug
ERROR: No such bug: Not_a_bug



# Check that the previous bug was not affected
trit showbug parse_with_question_mark_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..6527bde 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -528,3 +528,6 @@ class URL(object):
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str.replace('?',''))
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: FAIL
test_has_query_params: FAIL
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: FAIL
test_path_extraction: FAIL
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: FAIL
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ERROR
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (errors=1, failures=10)



# Now create another bug
git checkout purl/url.py
cat >> purl/url.py <<'END'
origparse=parse
def parse(url_str):
    return origparse(url_str[:-1])
END
trit addbug parse_with_last_symbol_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..d1085fe 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -521,10 +521,13 @@ class URL(object):
         return cls(**args)
 
     @classmethod
     def from_string(cls, url_str):
         """
         Factory method to create a new instance based on a passed string
 
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str[:-1])
====================================================

Please review the bug code above and make sure that

 1) there are no changes unrelated to the intended bug

 2) all changes are in the code rather than in tests

Do you want to proceed with 'trit addbug'? (y/Y/n/N)
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (failures=5)
====================================================

Please review the effect of the bug on the tests above.

Have all tests that this bug should have broken failed? (y/Y/n/N)



git status --porcelain --branch
## master...origin/master



trit bugs
parse_with_last_symbol_dropped  (failures=5)
parse_with_question_mark_dropped  (errors=1, failures=10)



trit showbug parse_with_last_symbol_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..d1085fe 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -528,3 +528,6 @@ class URL(object):
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str[:-1])
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (failures=5)



trit showbug parse_with_question_mark_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..6527bde 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -528,3 +528,6 @@ class URL(object):
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str.replace('?',''))
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: FAIL
test_has_query_params: FAIL
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: FAIL
test_path_extraction: FAIL
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: FAIL
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ERROR
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (errors=1, failures=10)



# Check that the newly added bug's status
trit checkbugs parse_with_question_mark_dropped
The effect of the bug 'parse_with_question_mark_dropped' is as expected



# Verify that 'trit checkbugs' doesn't resurrect the bug
git status --porcelain --branch
## master...origin/master



# Rename a couple of tests
sed -i -e 's/test_subdomain/test_sub_domain/' tests/url_tests.py
git status --porcelain --branch
## master...origin/master
 M tests/url_tests.py



# 'trit checkbugs' should detect that the effect
# of the given bug on the tests has changed
trit checkbugs parse_with_question_mark_dropped
The effect of the bug 'parse_with_question_mark_dropped' has changed. Diff follows:
 test_domain: ok
 test_has_actual_param: FAIL
 test_has_query_params: FAIL
 test_has_query_params_negative: ok
 test_host: ok
 test_invalid_subdomain_raises_indexerror: ok
 test_netloc: ok
 test_parameter_extraction: FAIL
 test_parameter_extraction_is_none_if_not_found: ok
 test_parameter_extraction_with_default: ok
 test_path: FAIL
 test_path_extraction: FAIL
 test_path_extraction_can_take_default_value: ok
 test_path_extraction_returns_none_if_index_too_large: ok
 test_path_segments: FAIL
 test_port_defaults_to_none: ok
 test_query: FAIL
 test_query_param_as_list: FAIL
 test_query_params: FAIL
 test_relative: FAIL
 test_remove_query_param: ERROR
 test_scheme: ok
-test_subdomain: ok
-test_subdomains: ok
+test_sub_domain: ok
+test_sub_domains: ok
 ------------
 Ran 24 tests
 FAILED (errors=1, failures=10)



# 'trit checkbugs' should preserve the local changes
git status --porcelain --branch
## master...origin/master
 M tests/url_tests.py



# Without arguments, 'trit checkbugs' should check all the bugs
trit checkbugs
The effect of the bug 'parse_with_last_symbol_dropped' has changed. Diff follows:
 test_domain: ok
 test_has_actual_param: ok
 test_has_query_params: ok
 test_has_query_params_negative: ok
 test_host: ok
 test_invalid_subdomain_raises_indexerror: ok
 test_netloc: ok
 test_parameter_extraction: FAIL
 test_parameter_extraction_is_none_if_not_found: ok
 test_parameter_extraction_with_default: ok
 test_path: ok
 test_path_extraction: ok
 test_path_extraction_can_take_default_value: ok
 test_path_extraction_returns_none_if_index_too_large: ok
 test_path_segments: ok
 test_port_defaults_to_none: ok
 test_query: FAIL
 test_query_param_as_list: FAIL
 test_query_params: FAIL
 test_relative: FAIL
 test_remove_query_param: ok
 test_scheme: ok
-test_subdomain: ok
-test_subdomains: ok
+test_sub_domain: ok
+test_sub_domains: ok
 ------------
 Ran 24 tests
 FAILED (failures=5)
The effect of the bug 'parse_with_question_mark_dropped' has changed. Diff follows:
 test_domain: ok
 test_has_actual_param: FAIL
 test_has_query_params: FAIL
 test_has_query_params_negative: ok
 test_host: ok
 test_invalid_subdomain_raises_indexerror: ok
 test_netloc: ok
 test_parameter_extraction: FAIL
 test_parameter_extraction_is_none_if_not_found: ok
 test_parameter_extraction_with_default: ok
 test_path: FAIL
 test_path_extraction: FAIL
 test_path_extraction_can_take_default_value: ok
 test_path_extraction_returns_none_if_index_too_large: ok
 test_path_segments: FAIL
 test_port_defaults_to_none: ok
 test_query: FAIL
 test_query_param_as_list: FAIL
 test_query_params: FAIL
 test_relative: FAIL
 test_remove_query_param: ERROR
 test_scheme: ok
-test_subdomain: ok
-test_subdomains: ok
+test_sub_domain: ok
+test_sub_domains: ok
 ------------
 Ran 24 tests
 FAILED (errors=1, failures=10)



# Drop the local change
git checkout tests/url_tests.py



# Now let's do some 'development' and checkin some code
echo '#' >> purl/template.py
git commit -qm "Non functional change in purl/template.py" purl/template.py



# 'trit bugs' must still report the same bugs
trit bugs
parse_with_last_symbol_dropped  (failures=5)
parse_with_question_mark_dropped  (errors=1, failures=10)



# An attempt to add a bug after the code has changed should be rejected
git checkout purl/url.py
sed -i -e 's/urlparse(url_str)/urlparse(url_str.replace("www", "abc"))/' purl/url.py
trit addbug www_replaced_with_abc
ERROR: Code has evolved since last 'trit init/catchup'.
ERROR: Run 'trit catchup' first."



# No traces of the failed attempt to add a bug should show up
trit bugs
parse_with_last_symbol_dropped  (failures=5)
parse_with_question_mark_dropped  (errors=1, failures=10)



# 'trit catchup' should not require any intervention in this case
trit catchup
++++++++++++++++ Migrating the trit init commit +++++++++++++++++++++
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: ok
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: ok
test_query_param_as_list: ok
test_query_params: ok
test_relative: ok
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
OK

Please review the test run results above and
make sure that all tests of interest are listed.

Do you want to proceed with 'trit init'? (y/Y/n/N)
++++++++++++++++ Migrating bug parse_with_question_mark_dropped ++++++++++++++++++++++
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: FAIL
test_has_query_params: FAIL
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: FAIL
test_path_extraction: FAIL
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: FAIL
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ERROR
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (errors=1, failures=10)
====================================================

++++++++++++++++ Migrating bug parse_with_last_symbol_dropped ++++++++++++++++++++++
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (failures=5)
====================================================




# No new branches should appear as a result of completed 'trit catchup'
git branch --list
* master
  master.trit



# No new tags should appear as a result of completed 'trit catchup'
git tag -l '*trit*' -n
master.trit.root Root of the trit branch for master



# No bugs should disappear as a result of 'trit catchup'
trit bugs
parse_with_last_symbol_dropped  (failures=5)
parse_with_question_mark_dropped  (errors=1, failures=10)



# 'trit checkbugs' should work normally after 'trit catchup'
trit checkbugs
The effect of the bug 'parse_with_last_symbol_dropped' has changed. Diff follows:
-test_domain: ok
+test_domain: FAIL
 test_has_actual_param: ok
 test_has_query_params: ok
 test_has_query_params_negative: ok
-test_host: ok
+test_host: FAIL
 test_invalid_subdomain_raises_indexerror: ok
-test_netloc: ok
+test_netloc: FAIL
 test_parameter_extraction: FAIL
 test_parameter_extraction_is_none_if_not_found: ok
 test_parameter_extraction_with_default: ok
 test_path: ok
 test_path_extraction: ok
 test_path_extraction_can_take_default_value: ok
 test_path_extraction_returns_none_if_index_too_large: ok
 test_path_segments: ok
 test_port_defaults_to_none: ok
 test_query: FAIL
 test_query_param_as_list: FAIL
 test_query_params: FAIL
 test_relative: FAIL
-test_remove_query_param: ok
+test_remove_query_param: FAIL
 test_scheme: ok
-test_subdomain: ok
-test_subdomains: ok
+test_subdomain: FAIL
+test_subdomains: FAIL
 ------------
 Ran 24 tests
-FAILED (failures=5)
+FAILED (failures=11)
The effect of the bug 'parse_with_question_mark_dropped' has changed. Diff follows:
-test_domain: ok
+test_domain: FAIL
 test_has_actual_param: FAIL
 test_has_query_params: FAIL
 test_has_query_params_negative: ok
-test_host: ok
+test_host: FAIL
 test_invalid_subdomain_raises_indexerror: ok
-test_netloc: ok
+test_netloc: FAIL
 test_parameter_extraction: FAIL
 test_parameter_extraction_is_none_if_not_found: ok
 test_parameter_extraction_with_default: ok
 test_path: FAIL
 test_path_extraction: FAIL
 test_path_extraction_can_take_default_value: ok
 test_path_extraction_returns_none_if_index_too_large: ok
 test_path_segments: FAIL
 test_port_defaults_to_none: ok
 test_query: FAIL
 test_query_param_as_list: FAIL
 test_query_params: FAIL
 test_relative: FAIL
 test_remove_query_param: ERROR
 test_scheme: ok
-test_subdomain: ok
-test_subdomains: ok
+test_subdomain: FAIL
+test_subdomains: FAIL
 ------------
 Ran 24 tests
-FAILED (errors=1, failures=10)
+FAILED (errors=1, failures=15)



# Now 'trit addbug' should not complain of anything
trit addbug www_replaced_with_abc
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..967bd8f 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -95,21 +95,21 @@ def unicode_urlencode(query, doseq=True):
     encoded_query = dict(pairs)
     xx = urlencode(encoded_query, doseq)
     return xx
 
 
 def parse(url_str):
     """
     Extract all parts from a URL string and return them as a dictionary
     """
     url_str = to_unicode(url_str)
-    result = urlparse(url_str)
+    result = urlparse(url_str.replace("www", "abc"))
     netloc_parts = result.netloc.split('@')
     if len(netloc_parts) == 1:
         username = password = None
         host = netloc_parts[0]
     else:
         user_and_pass = netloc_parts[0].split(':')
         if len(user_and_pass) == 2:
             username, password = user_and_pass
         elif len(user_and_pass) == 1:
             username = user_and_pass[0]
====================================================

Please review the bug code above and make sure that

 1) there are no changes unrelated to the intended bug

 2) all changes are in the code rather than in tests

Do you want to proceed with 'trit addbug'? (y/Y/n/N)
========= Effect of the bug on the tests ===========
test_domain: FAIL
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: FAIL
test_invalid_subdomain_raises_indexerror: ok
test_netloc: FAIL
test_parameter_extraction: ok
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: ok
test_query_param_as_list: ok
test_query_params: ok
test_relative: ok
test_remove_query_param: FAIL
test_scheme: ok
test_subdomain: FAIL
test_subdomains: FAIL
------------
Ran 24 tests
FAILED (failures=6)
====================================================

Please review the effect of the bug on the tests above.

Have all tests that this bug should have broken failed? (y/Y/n/N)



git branch --list
* master
  master.trit



trit bugs
www_replaced_with_abc  (failures=6)
parse_with_last_symbol_dropped  (failures=5)
parse_with_question_mark_dropped  (errors=1, failures=10)



trit showbug www_replaced_with_abc
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..967bd8f 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -102,7 +102,7 @@ def parse(url_str):
     Extract all parts from a URL string and return them as a dictionary
     """
     url_str = to_unicode(url_str)
-    result = urlparse(url_str)
+    result = urlparse(url_str.replace("www", "abc"))
     netloc_parts = result.netloc.split('@')
     if len(netloc_parts) == 1:
         username = password = None
========= Effect of the bug on the tests ===========
test_domain: FAIL
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: FAIL
test_invalid_subdomain_raises_indexerror: ok
test_netloc: FAIL
test_parameter_extraction: ok
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: ok
test_query_param_as_list: ok
test_query_params: ok
test_relative: ok
test_remove_query_param: FAIL
test_scheme: ok
test_subdomain: FAIL
test_subdomains: FAIL
------------
Ran 24 tests
FAILED (failures=6)



trit showbug parse_with_last_symbol_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..d1085fe 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -528,3 +528,6 @@ class URL(object):
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str[:-1])
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: ok
test_has_query_params: ok
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: ok
test_path_extraction: ok
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: ok
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ok
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (failures=5)



trit showbug parse_with_question_mark_dropped
=============== Bug code (as diff) =================
diff --git a/purl/url.py b/purl/url.py
index b81e056..6527bde 100644
--- a/purl/url.py
+++ b/purl/url.py
@@ -528,3 +528,6 @@ class URL(object):
         This method is deprecated now
         """
         return cls(url_str)
+origparse=parse
+def parse(url_str):
+    return origparse(url_str.replace('?',''))
========= Effect of the bug on the tests ===========
test_domain: ok
test_has_actual_param: FAIL
test_has_query_params: FAIL
test_has_query_params_negative: ok
test_host: ok
test_invalid_subdomain_raises_indexerror: ok
test_netloc: ok
test_parameter_extraction: FAIL
test_parameter_extraction_is_none_if_not_found: ok
test_parameter_extraction_with_default: ok
test_path: FAIL
test_path_extraction: FAIL
test_path_extraction_can_take_default_value: ok
test_path_extraction_returns_none_if_index_too_large: ok
test_path_segments: FAIL
test_port_defaults_to_none: ok
test_query: FAIL
test_query_param_as_list: FAIL
test_query_params: FAIL
test_relative: FAIL
test_remove_query_param: ERROR
test_scheme: ok
test_subdomain: ok
test_subdomains: ok
------------
Ran 24 tests
FAILED (errors=1, failures=10)



trit checkbugs parse_with_last_symbol_dropped www_replaced_with_abc
The effect of the bug 'parse_with_last_symbol_dropped' is as expected
The effect of the bug 'www_replaced_with_abc' is as expected



# 'trit stat' reports statistics of test failures due to different bugs
trit stat
test_domain: failures=1 errors=0
test_has_actual_param: failures=1 errors=0
test_has_query_params: failures=1 errors=0
test_has_query_params_negative: failures=0 errors=0
test_host: failures=1 errors=0
test_invalid_subdomain_raises_indexerror: failures=0 errors=0
test_netloc: failures=1 errors=0
test_parameter_extraction: failures=2 errors=0
test_parameter_extraction_is_none_if_not_found: failures=0 errors=0
test_parameter_extraction_with_default: failures=0 errors=0
test_path: failures=1 errors=0
test_path_extraction: failures=1 errors=0
test_path_extraction_can_take_default_value: failures=0 errors=0
test_path_extraction_returns_none_if_index_too_large: failures=0 errors=0
test_path_segments: failures=1 errors=0
test_port_defaults_to_none: failures=0 errors=0
test_query: failures=2 errors=0
test_query_param_as_list: failures=2 errors=0
test_query_params: failures=2 errors=0
test_relative: failures=2 errors=0
test_remove_query_param: failures=1 errors=1
test_scheme: failures=0 errors=0
test_subdomain: failures=1 errors=0
test_subdomains: failures=1 errors=0
