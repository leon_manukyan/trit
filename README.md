# What is `trit` for?

`trit` is a tool for enabling a general approach/methodology to assuring the
quality of tests.

The specification of a test can be reduced to two clauses:

1. It must pass for correct implementation of the feature being tested, and
2. It must fail for incorrect/broken implementation of the feature being tested

To the best of my knowledge, while the requirement 1. is routinely being
exercised, little attention is being paid to requirement 2.

Typically

- a test-suite is created,
- the code is run against it,
- any failures (because of bugs either in the code or in the tests) are fixed
- and we arrive at a situation when we believe that our code and tests are good.

The actual situation may be that (some of) the tests contain bugs that (would)
prevent them from catching bugs in the code. Therefore, seeing tests pass
shouldn't suggest much tranquility to the person caring about the quality of
the system, until they are confident that the tests are indeed able to detect
the problems they were designed against[1]. And a simple way to do it is to
actually introduce such problems and check that they don't remain unnoticed by
the tests!

In TDD (test-driven development), this idea is followed only partially - the
recommendation is to add the test before the code, see it fail (it should,
since there is no code yet) and then fix it by writing the code. But failure of
a test because of missing code doesn't automatically mean that it will also
fail in case of buggy code!

So the quality of a test suite can be measured as a percentage of bugs that it
would be capable of detecting. Any reasonable bug[2] that escapes a test-suite
suggests a new test case covering that scenario (or, if the test suite should
have caught that bug, a bug in the test suite is uncovered). This also means
that every test of the suite must be able to catch at least one bug (otherwise,
that test is completely pointless).

So, `trit` comes to solve the problem of *Testing the Tests* (TtT). Its name comes
from that abbreviation (TtT = **Tri**ple **T**).

# Getting started

To quickly get started with `trit`, clone this repository and try its demo flow (see below).

> ### Note
>
> Since this repository contains a submodule (that is used in its test and demo flows), you
> either have to clone it using the `--recursive` switch (`git clone --recursive`) or run
> the `git submodule update --init` command after cloning in a regular/non-recursive way.

## Demo flow

Run the demo flow by simply executing the `run_demo` script.

Prerequisites:

- The `tests/purl` submodule must be initialized (see above Note)
- bash (4.3+) and standard Unix utilities (sed, cat, etc)
- git (2.7+)
- Python 2.7
- nosetests (a tool for discovery and running of Python unittests). Under Debian Linux distributions it can be obtained as follows:

        apt install python-nose

    Or via `pip`:

        pip install nose

# Implementation notes

Currently `trit` works only inside git repositories. With current implementation, it
stores the artificial/simulated bugs on branches of their own. For every development
branch, `trit` creates a separate branch where the buggy code lives. Every bug lives
on a separate commit. Information about bugs is stored in the commit messages.

# Commands

`trit`'s commands are briefly summarized below:

```
  trit init [<testing_script>] - sets up the current branch for use with trit
  trit addbug <bugid>          - adds a trit bug to the current branch
  trit bugs                    - lists bugs added to the current branch
  trit showbug <bugid>         - shows the code and effect of the bug
  trit checkbugs [<bugid> ...] - checks the current effect of the listed bugs
  trit stat                    - reports statistics about tests
  trit catchup                 - updates bugs after changes to the codebase
```

### trit init

Synopsis:

    trit init [<testing_script>]

Before `trit` can be used on a branch, `trit init` must be called on that branch.
`trit init` requires a script for running the tests. The script must be located
inside the project and meet the requirements described in the section
**Requirements imposed on the test running script**. The name/location of the
test running script defaults to `./run_trit_tests`.

`trit init` executes the test running script and expects all tests to pass. If there
are any failures - `trit init` exits with an error.

`trit init` requires that there are no modifications to files tracked by git. This
is intended to ensure that the cleanness of tests corresponds to the current state
of the branch rather than can be a result of local changes.

If the test running script provided to `trit init` is not tracked by git, then as a
result of executing `trit init` (provided that it succeeds) the script disappears
from the working tree. This is normal behavior - the script is saved on `trit`'s own
branch and is removed from the working tree lest it be littered by stuff unrelated to
the regular development activities.

### trit addbug

Synopsis:

    trit addbug <bugid>

Blablabla
Blablabla
Blablabla
Blablabla

### trit bugs

Synopsis:

    trit bugs

Blablabla
Blablabla
Blablabla
Blablabla

### trit showbug

Synopsis:

    trit showbug <bugid>

Blablabla
Blablabla
Blablabla
Blablabla

### trit checkbugs

Synopsis:

    trit checkbugs [<bugid> ...]

Blablabla
Blablabla
Blablabla
Blablabla

### trit stat

Synopsis:

    trit stat

Blablabla
Blablabla
Blablabla
Blablabla

### trit catchup

Synopsis:

    trit catchup

Blablabla
Blablabla
Blablabla
Blablabla

# Requirements imposed on the test running script

In this section the script is assumed to be `./run_trit_tests`, though,
if you don't omit the optional parameter when running `trit init`, you can
name the script as you like and place it inside your project where you like.

## Synopsis:

    ./run_trit_tests

Currently `trit` calls the test running script without any arguments. An
anticipated enhancement of the use model will allow to associate with each
bug only a subset of the full set of tests. Then the test running script
will need to provide means for specifying the list of tests to run.

## Execution

The test running script must run the tests using the source found in the
working tree. That is, if necessary, it must also build all relevant targets
that became out-of-date because of changes made to the working tree since the
previous build/test cycle.

> A problem encountered with the demo flow of this project was a mysterious
> randomness in the outcome of the `checkbugs` step when the flow was run in
> unattended/batch mode. It turned out that the python code was byte-compiled
> and the resulting `.pyc` files were cached. In batch mode of the demo, the
> interval between running the tests for one bug and then modifying the working
> tree (when switching to the next bug) was so short that the Python source
> files didn't appear newer than the respective `.pyc` files. As a result, the
> next round of testing was actually being performed with the previous state of
> the code, i.e. using the previous bug. The problem was fixed (6b00925) by
> disabling byte compilation in the test running script.

## Output Format

The test running script must report the information about test outcomes in the following format:

```
<test1_id>: <test_status>
<test2_id>: <test_status>
...
<testN_id>: <test_status>
------------
Ran <N> tests
<test_suite_status>
```

where

```
<test_status> := ok | ERROR | FAIL
<test_suite_status> := OK
                       | FAILED (failures=<count>)
                       | FAILED (errors=<count>)
                       | FAILED (errors=<count>, failures=<count>)
```

Test ids must not contain whitespace.

Example output:

```
test_no_equals_sign_means_empty_string: ok
test_list_extraction: ok
test_username_extraction: FAIL
test_username_in_unicode_repr: FAIL
test_auth_in_netloc: FAIL
test_port_in_netloc: FAIL
test_passwordless_netloc: ok
test_unicode_username_and_password: FAIL
test_unicode_username_only: ok
test_port_for_https_url: ok
------------
Ran 10 tests
FAILED (failures=5)
```

## Exit status

The test running script must exit with a 0 status only if there were no test
failures/errors. In all other cases it must exit with a non-zero status.

---

## Footnotes

[1] A more generalized version of this statement is true for a wider range of
systems/situations (all typically having to do with security/safety):

> A system designed against certain events must be routinely tested against
> such events, otherwise it is prone to degradation down to complete inability to
> react against the events of interest.

Just an example - do you have a fire alarm system at home? When did you witness
it work last time? What if it stays silent during fire too? Go make some smoke
in the room right now!

[2] Within the scope of this methodology, a back-door like bug (e.g. when the
feature misbehaves only if the passed in URL is equal to
https://www.formatmyharddrive.com/?confirm=yesofcourse) is not a reasonable one